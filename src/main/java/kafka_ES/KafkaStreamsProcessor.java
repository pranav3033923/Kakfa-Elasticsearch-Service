package kafka_ES;

import org.apache.http.Header;
import org.apache.http.HttpHost;
import org.apache.http.RequestLine;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.codehaus.jettison.json.JSONObject;
import org.elasticsearch.client.*;
import org.elasticsearch.common.xcontent.XContentType;

import java.io.IOException;
import java.io.InputStream;
import java.time.Duration;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;

public class KafkaStreamsProcessor {
    private static final Logger logger = LogManager.getLogger(KafkaStreamsProcessor.class);

    private static final String SECRETS_FILE = "client.properties";
    private static final String CLIENT_FILE = "es.properties";
    private static final String KAFKABOOTSTRAPSERVERS = "bootstrap.servers";
    private static final String KAFKATOPIC = "kafka.topic";
    private static final String ELASTICSEARCHHOST = "elasticsearch.host";
    private static final int ELASTICSEARCHPORT = 9200;
    private static final String ELASTICSEARCHINDEX = "elasticsearch.index";
    private static final String ELASTICSEARCHPATH = "elasticsearch.path";
    private static final String ELASTICSEARCHUSERNAME = "elasticsearch.username";
    private static final String ELASTICSEARCHPASSWORD = "elasticsearch.password";
    public static String KAFKA_BOOTSTRAP_SERVERS="";
    public static String KAFKA_TOPIC="";
    public static String ELASTICSEARCH_HOST="";
    public static int ELASTICSEARCH_PORT=9243;
    public static String ELASTICSEARCH_INDEX="";
    public static String ELASTICSEARCH_PATH="";
    public static String ELASTICSEARCH_USERNAME="";
    public static String ELASTICSEARCH_PASSWORD="";


    public static void main(String[] args) {


//        Loading properties from secret.properties
        Properties properties = loadProperties(CLIENT_FILE);
        ELASTICSEARCH_HOST=properties.getProperty(ELASTICSEARCHHOST);
        ELASTICSEARCH_INDEX=properties.getProperty(ELASTICSEARCHINDEX);
        ELASTICSEARCH_PATH=properties.getProperty(ELASTICSEARCHPATH);
        ELASTICSEARCH_USERNAME=properties.getProperty(ELASTICSEARCHUSERNAME);
        ELASTICSEARCH_PASSWORD=properties.getProperty(ELASTICSEARCHPASSWORD);



//        setting up properties of Kafka cloud
        Properties props= loadProperties(SECRETS_FILE);
        KAFKA_BOOTSTRAP_SERVERS=props.getProperty(KAFKABOOTSTRAPSERVERS);
        KAFKA_TOPIC=props.getProperty(KAFKATOPIC);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "kafka-es-connect");
        props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,StringDeserializer.class);




        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Arrays.asList(KAFKA_TOPIC));

        // Running Kafka Consumer stream
        while (true) {

            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));
            for (ConsumerRecord<String, String> record : records) {


                storeInElasticsearch(record.value());
                ThreadContext.put("statusCode", "200");
                logger.info("Service Kafka - ES Success:2.0 -  Kafka Stream captured a Doc[--] change via CDC");
                ThreadContext.clearMap();
            }
        }
    }

    private static Properties loadProperties(final String configFile) {
        // Loading secrets from src/resources/secrets.properties
        Properties properties = new Properties();
        try (InputStream inputStream = KafkaStreamsProcessor.class.getClassLoader().getResourceAsStream(configFile)) {
            properties.load(inputStream);
        } catch (IOException e) {
            ThreadContext.put("statusCode", "400");
            logger.error("Service Kafka - ES Failed -  An error occurred while extracting secrets from .prop file");
            ThreadContext.clearMap();
            throw new RuntimeException("Service Kafka - ES Failed -  An error occurred while extracting secrets from .prop file");
        }
        return properties;
    }

    public static RestHighLevelClient createClient() {
        // ES High level client setup
        RestClientBuilder builder = RestClient.builder(new HttpHost(ELASTICSEARCH_HOST, ELASTICSEARCH_PORT, "https"))
                .setHttpClientConfigCallback(new RestClientBuilder.HttpClientConfigCallback() {
                    @Override
                    public HttpAsyncClientBuilder customizeHttpClient(HttpAsyncClientBuilder httpClientBuilder) {
                        return httpClientBuilder.setDefaultCredentialsProvider(createCredentialsProvider(ELASTICSEARCH_USERNAME, ELASTICSEARCH_PASSWORD));
                    }
                })
                .setRequestConfigCallback(new RestClientBuilder.RequestConfigCallback() {
                    @Override
                    public RequestConfig.Builder customizeRequestConfig(RequestConfig.Builder requestConfigBuilder) {
                        return requestConfigBuilder.setConnectTimeout(5000).setSocketTimeout(60000);
                    }
                });


        RestHighLevelClient client = new RestHighLevelClient(builder);

        return client;
    }

    private static CredentialsProvider createCredentialsProvider(String username, String password) {
        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(username, password));
        return credentialsProvider;
    }
    private static void storeInElasticsearch(String message) {
        // Storing message in ES


        try (RestHighLevelClient client = createClient()) {
            JSONObject json = new JSONObject(message);
            String id = json.getJSONObject("_id").getString("$oid");
            json.remove("_id"); // Remove the _id field from the document
            Request request = new Request(
                    "PUT",
                    ELASTICSEARCH_PATH+id);
            request.setJsonEntity(json.toString());

            Response response = client.getLowLevelClient().performRequest(request);

            int statusCode = response.getStatusLine().getStatusCode();
            String responseBody = EntityUtils.toString(response.getEntity());

            ThreadContext.put("statusCode", String.valueOf(statusCode));
            logger.info("Service Kafka - ES Success:1.0 - Doc[--] successfully sent to ES");
            ThreadContext.clearMap();

        } catch (Exception e) {
            ThreadContext.put("statusCode", "400");
            logger.error("Service Kafka - ES Success:1.0 - Doc[--] successfully sent to ES");
            ThreadContext.clearMap();
            throw new RuntimeException("Service Kafka - ES Failed - An error occurred while sending Doc[--] to ES");
        }
    }
}
